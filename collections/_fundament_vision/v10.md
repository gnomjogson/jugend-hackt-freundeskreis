---
title: Automation of legal process
image: vision/icon_automation-of-legal-proceess.svg
---
In the digital domain, many legal processes can be automated, once templates are written.
By getting rid of unnecessary legal hold-ups, you save time and money.
