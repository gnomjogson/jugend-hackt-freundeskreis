/**
 * Author: CReich
 * Company: Rainbow Unicorn
 * Date: 14.06.2017
 * Created: 15:16
 **/
(function(window){

    Controller.prototype.constructor = Controller;
    Controller.prototype = {

    };

    var ref, $, $body, $header, alpakaMenuTimer, alpakaIndex;
    function Controller(jQuery){

        $ = jQuery;
        ref = this;
        alpakaIndex = 0;

        Logger.useDefaults();
        //Logger.setLevel(Logger.OFF);

        var browser = ref.getBrowser();
        $('body').addClass(browser.name.toLowerCase()).addClass('version-' + browser.version.toLowerCase());

        isMobile = ref.isMobileDevice();
        if(isMobile){
            $('body').addClass('mobile');
        } else $('body').addClass('desktop');

    };

    Controller.prototype.init = function(){

        Logger.log("Page initialized. isMobile: " + isMobile);

        $body = $('body');
        $header = $('.header');

        $('.nav-link.apply')
            .mouseover(function() {
                var $that = $(this);
                alpakaMenuTimer = setInterval(function(){
                    var $imgs = $that.find('img');
                    if(alpakaIndex < 4){
                        alpakaIndex++;
                    } else alpakaIndex = 0;
                    $imgs.each(function(){
                        var i = $(this).attr('data-index');
                        if(i == alpakaIndex){
                            $(this).addClass('active');
                        } else $(this).removeClass('active');
                    });
                }, 100);
        })
            .mouseout(function() {
                window.clearInterval(alpakaMenuTimer);
                var $imgs = $(this).find('img');
                $imgs.each(function(){
                    var i = $(this).attr('data-index');
                    if(i == 0){
                        $(this).addClass('active');
                    } else $(this).removeClass('active');
                });
            });

        $('.hamburger').click(function(){
            if($(this).hasClass('is-active')){
                $(this).removeClass('is-active') ;
            } else $(this).addClass('is-active') ;
        });

        $('.image-teaser img').one("load", function() {
            ref.resize();
        });

        ref.addEventHandlers();
        ref.resize();

    };

    Controller.prototype.addEventHandlers = function(){

        /*********************
        scroll event
        *********************/
        lastScrollTop = 0;
        $(window).scroll(function(event){

            if(ref.viewport().width <= 1150) return;

            var st = $(this).scrollTop();
            var hh = $header.height();

            if (st > lastScrollTop){
                // down
                if(st > 0){
                    //remove header
                }
            } else {
                // up
                if($header.hasClass('gone')){
                    //show header
                }
                if(st == hh) $header.removeClass('fixed');
            }
            lastScrollTop = st;
        });

        /*********************
        resize event
        *********************/
        var delay = (function(){
            var timer = 0;
            return function(callback, ms){
                clearTimeout (timer);
                timer = setTimeout(callback, ms);
            };
        })();

        $(window).resize(function () {
            delay(function () {
                ref.resize();
            }, 50);
        });
        ref.resize();
    };

    /*********************
    resize event handler
    *********************/
    Controller.prototype.resize = function(){
        $('.image-teaser').each(function(){
            var h = $(this).find('img').height();
            if($(this).find('.image-teaser-content').height() < h){
                $(this).find('.image-teaser-content').height(h);
            }
        });
    };

    /*********************
     mobile detect
     *********************/
    Controller.prototype.isMobileDevice = function(){
        return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
    };

    /*********************
    viewport().width, viewport().height
    *********************/
    Controller.prototype.viewport = function()
    {
        var e = window, a = 'inner';
        if (!('innerWidth' in window )) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
    };

    /*********************
    get browser type + version
    *********************/
    Controller.prototype.getBrowser = function()
    {
        var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if(/trident/i.test(M[1])){
            tem=/\brv[ :]+(\d+)/g.exec(ua) || [];
            return {name:'IE',version:(tem[1]||'')};
        }
        if(M[1]==='Chrome'){
            tem=ua.match(/\bOPR\/(\d+)/)
            if(tem!=null)   {return {name:'Opera', version:tem[1]};}
        }
        M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
        if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
        return {
            name: M[0],
            version: M[1]
        };
    };

    window.Controller = Controller;

}(window));
